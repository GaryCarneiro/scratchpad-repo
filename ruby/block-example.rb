#!/usr/bin/env ruby
# http://programming.oreilly.com/2014/02/why-ruby-blocks-exist.html
#
#
def calculate_tax(income)
    tax_rate = 0.2
    yield income * tax_rate
end
